<br />
<div align="center">
  <a href="https://gitlab.com/nfef-fcul/website/website">
    <img src="static/images/logo.png" alt="Logo" width="80" height="95">
  </a>

  <h3>NFEF-FCUL</h3>

  <p>
    Website do Núcleo de Física e de Engenharia Física da Faculdade de Ciências da Universidade de Lisboa
  </p>
</div>


## Sobre o NFEF-FCUL
O Núcleo de Física e de Engenharia Física da Faculdade de Ciências da Universidade de Lisboa foi criado a 19 de Maio de 2016. 

Conta já com a quarta geração e a nossa missão é garantir a representação dos estudantes de Física e Engenharia Física na faculdade, bem como permitir uma interação entre os alunos e o departamento.

O NFEF-FCUL tem, também, como objetivo complementar a experiência académica com eventos socais e pedagógicos. Dentro destes é importante destacar os churrascos bianuais, os diversos workshops, o programa do mentorado e o maior evento do núcleo, "Física Fora da Academia".


## Contribuir
Existem muitas maneiras de contribuir ao desenvolvimento do website, encontra a que se enquadra melhor com as tuas capacidades e interesses e abre in issue/pull request no repositório.

Exemplos de contribuições que nós adoramos:

- **Código**
- **Report de bugs**
- **Revisão de código**
- **Traduções**
- **Melhorias a UI**
- **Elaborar as recomendações abaixo**

## Recomendações para o workflow (Windows)
No [guia](https://docs.microsoft.com/en-us/windows/wsl/setup/environment#set-up-your-linux-username-and-password) da Microsoft é possível encontrar informação adicional e/ou complementar à que se segue.
### Instalar o [WSL2](https://docs.microsoft.com/en-us/windows/wsl/about) com Ubuntu
O WSL, Windows Subsystem for Linux, é uma forma de utilizar distribuições deste sistema operativo de forma simples e rápida. Utilizar Linux, para além de proporcionar algumas funcionalidades e comandos que facilitam a produção de código, irá servir de uma formar de manter o código organizado, separado de outros ficheiros no pc.
Ubuntu é uma distribuição de Linux muito popular pela sua facilidade de usar, mas que apresenta todas as ferramentas que iremos precisar.

O comando necessário para instalação é o seguinte.
```
wsl --install -d Ubuntu
wsl --set-default-version 2
```
Para verificar a versão do WSL, o comando é o seguinte.
```
wsl -l -v
```
Se a versão de alguma distribuição de Linux não for a 2, poderá ser atualizada com o comando seguinte, em que \<nome da distribuição\> terá de ser substituido.
```
wsl --set-version <nome da distribuição> 2
```
Ao utilizar a WSL, ter atenção à pasta onde se está a trabalhar. Por default a command line vai estar aberta na pasta do utilizador do Windows. Ver na documentação como alterar esta diretoria inicial.

#### Atualizar o windows para a versão mais recente
O WSL2 requer versões do Windows recentes. Windows 11 ou Windows 10 versão 2004 ou superior (Build 19041 ou superior). O WSL2 oferece várias vantagens em relação à primeira versão, como melhorias de desempenho, e, portanto, é recomendável utilizá-la.

#### Atualizar, configurar e utilizar o Ubuntu
Quando o Ubuntu é instalado, este irá pedir por um nome de utilizados e uma password que será utilizada para executar comandos como administrador. É importante não se esquecer dela.
É boa prática atualizar os pacotes instalados frequentemente. Para isto, utiliza-se o comando seguinte.
```
sudo apt update && sudo apt upgrade
```
Alguns comandos úteis são:
- Print Working Directory: ```pwd``` 
- List Directory Contents: ```ls```
-  Change Directory: ```cd```
- Make Directory: ```mkdir``` 
- Remove: ```rm```
- Clear: ```clear```

Para mais informação usar o comando ```help``` ou ```info <comando>``` em que \<comando\> poderá ser por exemplo um dos acima.
Para além disso, em geral, 'ctrl + c' irá cancelar a operação que esteja a decorrer.

---

### Instalar o [Windows Terminal](https://docs.microsoft.com/en-us/windows/terminal/install)
O Windows Terminal é um programa que permite mais facilmente utilizar a command line, oferecendo vários comandos e muitas possibilidades de customização. É possível facilmente configurar a diretoria inicial entre outras opções, nos settings. No entanto, este programa não é estritamente necessário.

---

### Utilizar o [Visual Studio Code](https://code.visualstudio.com/)
VS Code é um IDE da Microsoft que oferece muitas funcionalidades úteis. É possível instalar extensões para qualquer linguagem que seja necessária para além de ter uma ótima integração com git e o WSL.
#### Instalar o pack de extensões [Remote Development](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack)
Estas extensões permitirão o uso do IDE no ambiente Windows enquanto que os ficheiros continuarão a estar guardados no WSL.

Para abrir o VS Code numa pasta da WSL, utiliza-se o comando seguinte.
```
code .
```
Se por alguma razão for necessário, é possível aceder ao explorador de ficheiros através do IDE ou através do comando abaixo.
```
explorer.exe .
```
#### Outras extensões úteis
- [Better TOML](https://marketplace.visualstudio.com/items?itemName=bungcip.better-toml)
- [Markdown Preview Enhanced](https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced)
- [Hugo Language and Syntax Support](https://marketplace.visualstudio.com/items?itemName=budparr.language-hugo-vscode)

---

### Instalar [git](https://git-scm.com/doc) e [hugo](https://gohugo.io/documentation/)
Para trabalhar no site será necessário instalar git e Hugo. Os comandos necessários para tal são os seguintes.
```
sudo apt install git
sudo apt install hugo
```
---

### Criar um par de [chaves SSH](https://docs.gitlab.com/ee/ssh/)
Para verificar se já existe uma chave no sistema, na diretoria 'home' utilizamos os comandos seguintes.
```
cd .ssh
ls
```
Caso não exista a pasta '.ssh' ou esta não inclua dois ficheiros com o mesmo nome em que um deles termine em '.pub', será necessário cria um novo par de chaves.
Para isto, é preciso correr o comando seguinte, em que \<comment\> poderá ser por exemplo um email.
```
ssh-keygen -t ed25519 -C  "<comment>"
```
De seguida, premir enter até ao fim. Guardar a imagem de recuperação num ficheiro de texto é recomendável.
Agora, será necessário adicionar a chave pública ao [gitlab](https://docs.gitlab.com/ee/ssh/#add-an-ssh-key-to-your-gitlab-account). Primeiro, passamos a chave para o clipboard com o comando seguinte. Poderá ser necessário instalar o pacote 'xclip'. O comando para isto aparecerá na linha de comando.
```
xclip -sel clip < ~/.ssh/id_ed25519.pub
```
Continuando, no 'Gitlab>Menu>Preferences>SSH Keys', colar na caixa 'Key' os conteúdos da chave, incluindo o comment (ctrl + v). Na caixa 'Title', um comentário que pode ser uma identificação do computador onde está a chave privada, 'Laptop' ou 'Home', por exemplo.
Finalmente, definir uma data de validade e adicionar a chave.

---

### Descarregar e correr o código
Para clonar o repositório de git para o computador, será necessário correr os seguintes comandos, onde \<folder-name\> é o nome da pasta onde se quer guardar os ficheiros do repositório.
```
git clone git@gitlab.com:nfef-fcul/website/website.git <folder-name>
git submodule update --init
```
Para clonar o repositório na pasta atual, os comandos são os seguintes.
```
git clone git@gitlab.com:nfef-fcul/website/website.git .
git submodule update --init
```

Para correr o código e observar as mudanças em tempo real, é necessário correr o comando seguinte e aceder ao link que aparecerá na linha de comando (ctrl + click).

```
hugo server --themesDir ./themes --bind $(hostname -I) --baseURL=http://$(hostname -I) --config $(echo $(find ./configs -iname *.toml) | sed 's/\s/,/g')
```

## Licença
Qualquer contribuição está sob licença [MIT](https://opensource.org/licenses/MIT).


## Website Construído Com
* [Hugo](https://gohugo.io/)
* [Elate](https://github.com/saey55/hugo-elate-theme)
